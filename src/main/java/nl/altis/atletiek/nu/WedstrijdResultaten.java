package nl.altis.atletiek.nu;

import java.util.ArrayList;
import java.util.List;

public class WedstrijdResultaten {

	private String wedstrijd;
	private String datum;
	private List<UitslagDeelnemer> uitslagen = new ArrayList<>();
	
	public String getWedstrijd() {
		return wedstrijd;
	}
	public void setWedstrijd(String wedstrijd) {
		this.wedstrijd = wedstrijd;
	}
	public List<UitslagDeelnemer> getUitslagen() {
		return uitslagen;
	}
	public void setUitslagen(List<UitslagDeelnemer> uitslagen) {
		this.uitslagen = uitslagen;
	}
	public String getDatum() {
		return datum;
	}
	public void setDatum(String datum) {
		this.datum = datum;
	}
	
	
}
