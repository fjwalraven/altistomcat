package nl.altis.atletiek.nu;

import java.util.HashSet;
import java.util.Set;

public enum OnderdeelAnu {

	_40M("40m", MeetEenheid.TIJD),
	_50M("50m", MeetEenheid.TIJD),
	_60M("60m", MeetEenheid.TIJD, Boolean.TRUE),
	_60M_HALVE_FINALE("60m_hf", MeetEenheid.TIJD, "60m", Ronde.HALVE_FINALES, Boolean.TRUE),
	_60M_FINALE("60m_f", MeetEenheid.TIJD, "60m", Ronde.FINALE, Boolean.TRUE),
	_80M("80m", MeetEenheid.TIJD, Boolean.TRUE),
	_80M_HALVE_FINALE("80m_hf", MeetEenheid.TIJD, "80m", Ronde.HALVE_FINALES, Boolean.TRUE),
	_80M_FINALE("80m_f", MeetEenheid.TIJD, "80m", Ronde.FINALE, Boolean.TRUE),
	_100M("100m", MeetEenheid.TIJD, Boolean.TRUE),
	_100M_WHEELERS("100mWh", MeetEenheid.TIJD, "100m", Boolean.TRUE),
	_100M_HALVE_FINALE("100m_hf", MeetEenheid.TIJD, "100m", Ronde.HALVE_FINALES, Boolean.TRUE),
	_100M_FINALE("100m_f", MeetEenheid.TIJD, "100m", Ronde.FINALE, Boolean.TRUE),
	_150M("150m", MeetEenheid.TIJD, Boolean.TRUE),
	_150M_FINALE("150m_f", MeetEenheid.TIJD,"150m", Ronde.FINALE, Boolean.TRUE),
	_200M("200m", MeetEenheid.TIJD, Boolean.TRUE),
	_200M_HALVE_FINALE("200m_hf", MeetEenheid.TIJD,"200m", Ronde.HALVE_FINALES, Boolean.TRUE),
	_200M_FINALE("200m_f", MeetEenheid.TIJD, "200m", Ronde.FINALE, Boolean.TRUE),
	_200M_WHEELERS("200mWh", MeetEenheid.TIJD, "200m", Boolean.TRUE),
	_300M("300m", MeetEenheid.TIJD),
	_300M_FINALE("300m_f", MeetEenheid.TIJD, "300m", Ronde.FINALE),
	_400M("400m", MeetEenheid.TIJD),
	_400M_HALVE_FINALE("400m_hf", MeetEenheid.TIJD, "400m", Ronde.HALVE_FINALES),
	_400M_FINALE("400m_f", MeetEenheid.TIJD, "400m", Ronde.FINALE),
	_400M_WHEELERS("400mWh", MeetEenheid.TIJD, "400m"),
	_600M("600m", MeetEenheid.TIJD),
	_800M("800m", MeetEenheid.TIJD),
	_800M_FINALE("800m_f", MeetEenheid.TIJD, "800m", Ronde.FINALE),
	_800M_WHEELERS("800mWh", MeetEenheid.TIJD, "800m"),
	_1000M("1000m", MeetEenheid.TIJD),
	_1MILE("1Mijl", MeetEenheid.TIJD),
	_1250M("1250m", MeetEenheid.TIJD),
	_1500M("1500m", MeetEenheid.TIJD),
	_1500M_FINALE("1500m_f", MeetEenheid.TIJD, "1500m", Ronde.FINALE),
	_2000M("2000m", MeetEenheid.TIJD),
	_3000M("3000m", MeetEenheid.TIJD),
	_3200M("3200m", MeetEenheid.TIJD),
	_5000M("5000m", MeetEenheid.TIJD),
	_10000M("10000m", MeetEenheid.TIJD),
	COOPERTEST("Meters", MeetEenheid.TIJD),
	COOPERTEST_2("Coop", MeetEenheid.TIJD),
	COOPERTEST_6M("Coop6", MeetEenheid.TIJD),
	COOPERTEST_60M("Coop60", MeetEenheid.TIJD),
	_50MH("50mH", MeetEenheid.TIJD),
	_60MH("60mH", MeetEenheid.TIJD, Boolean.TRUE),
	_60MH_HALVE_FINALE("60mH_hf", MeetEenheid.TIJD, "60mH", Ronde.HALVE_FINALES, Boolean.TRUE),
	_60MH_FINALE("60mH_f", MeetEenheid.TIJD, "60mH", Ronde.FINALE, Boolean.TRUE),
	_80MH("80mH", MeetEenheid.TIJD, Boolean.TRUE),
	_80MH_HALVE_FINALE("80mH_hf", MeetEenheid.TIJD, "80mH", Ronde.HALVE_FINALES, Boolean.TRUE),
	_80MH_FINALE("80mH_f", MeetEenheid.TIJD, "80mH", Ronde.FINALE, Boolean.TRUE),
	_100MH("100mH", MeetEenheid.TIJD, Boolean.TRUE),
	_100MH_HALVE_FINALE("100mH_hf", MeetEenheid.TIJD, "100mH", Ronde.HALVE_FINALES, Boolean.TRUE),
	_100MH_FINALE("100mH_f", MeetEenheid.TIJD, "100mH", Ronde.FINALE, Boolean.TRUE),
	_110MH("110mH", MeetEenheid.TIJD, Boolean.TRUE),
	_110MH_HALVE_FINALE("110mH_hf", MeetEenheid.TIJD, "110mH", Ronde.HALVE_FINALES, Boolean.TRUE),
	_110MH_FINALE("110mH_f", MeetEenheid.TIJD, "110mH", Ronde.FINALE, Boolean.TRUE),
	_200MH("200mH", MeetEenheid.TIJD),
	_300MH("300mH", MeetEenheid.TIJD),
	_300MH_FINALE("300mH_f", MeetEenheid.TIJD, "300mH", Ronde.FINALE),
	_400MH("400mH", MeetEenheid.TIJD),
	_400MH_FINALE("400mH_f", MeetEenheid.TIJD, "400mH", Ronde.FINALE),
	_1000M_STEEPLE("1000m SC", MeetEenheid.TIJD, "1000m SC"),
	_1500M_STEEPLE("1500m SC", MeetEenheid.TIJD, "1500m SC"),
	_2000M_STEEPLE("2000m SC", MeetEenheid.TIJD, "2000m SC"),
	_3000M_STEEPLE("3000m SC", MeetEenheid.TIJD, "3000m SC"),
	_3000M_SNELWANDELEN("3snelw", MeetEenheid.TIJD),
	_5000M_SNELWANDELEN("5snelw", MeetEenheid.TIJD),
	HOOGSPRINGEN("Hoog", MeetEenheid.AFSTAND,"hoog"),
	HOOGSPRINGEN_FINALE("Hoog_f", MeetEenheid.AFSTAND, "hoog", Ronde.FINALE),
	VERSPRINGEN("Ver", MeetEenheid.AFSTAND, "ver", Boolean.TRUE),
	VERSPRINGEN_FINALE("Ver_f", MeetEenheid.AFSTAND, "ver", Ronde.FINALE, Boolean.TRUE),
	HINKSTAPSPRINGEN("HSS", MeetEenheid.AFSTAND, "hss", Boolean.TRUE),
	HINKSTAPSPRINGEN_FINALE("HSS_f", MeetEenheid.AFSTAND, "hss", Ronde.FINALE, Boolean.TRUE),
	STAPSTAPSPRINGEN("SSS", MeetEenheid.AFSTAND),
	POLSTOKHOOGSPRINGEN("Polshoog", MeetEenheid.AFSTAND, "polshoog"),
	POLSTOKHOOGSPRINGEN_FINALE("Polshoog_f", MeetEenheid.AFSTAND, "polshoog", Ronde.FINALE),
	POLSTOKVERSPRINGEN("Polsver", MeetEenheid.AFSTAND),
	KOGEL("Kogel", MeetEenheid.AFSTAND,"kogel"),
	KOGEL_FINALE("Kogel_f", MeetEenheid.AFSTAND, "kogel", Ronde.FINALE),
	KOGELSLINGEREN("Kogelsl", MeetEenheid.AFSTAND, "kogelsl"),
	KOGELSLINGEREN_FINALE("Kogelsl_f", MeetEenheid.AFSTAND, "kogelsl", Ronde.FINALE),
	DISCUS("Discus", MeetEenheid.AFSTAND, "discus"),
	DISCUS_FINALE("Discus_f", MeetEenheid.AFSTAND, "discus", Ronde.FINALE),
	SPEERWERPEN("Speer", MeetEenheid.AFSTAND, "speer"),
	SPEERWERPEN_FINALE("Speer_f", MeetEenheid.AFSTAND, "speer", Ronde.FINALE),
	VORTEX("Vortex", MeetEenheid.AFSTAND),
	BAL("Bal", MeetEenheid.AFSTAND),
	GEWICHTWERPEN("Gewicht", MeetEenheid.AFSTAND, "gewicht"),
	MEERKAMP("Punten totaal", MeetEenheid.PUNTEN),
//	COMPETITIE("Team punten"),
	LICHAMELIJK_BEPERKT("club", MeetEenheid.PUNTEN);

	private String korteNaam;
	private String onderdeel;
	private Ronde ronde;
	private Boolean wind;
	private MeetEenheid meetEenheid;
	
	private static Set<String> onderdelen = new HashSet<>();
	private static Set<String> onderdelenMetWind = new HashSet<>();

	private OnderdeelAnu(String afkorting, MeetEenheid eenheid) {
		korteNaam = afkorting;
		onderdeel = afkorting;
		meetEenheid = eenheid;
	}
	
	private OnderdeelAnu(String afkorting, MeetEenheid eenheid, Boolean metWind) {
		korteNaam = afkorting;
		onderdeel = afkorting;
		wind = metWind;
		meetEenheid = eenheid;
	}
	
	private OnderdeelAnu(String afkorting, MeetEenheid eenheid, String ond) {
		korteNaam = afkorting;
		onderdeel = ond;
		meetEenheid = eenheid;
	}
	
	private OnderdeelAnu(String afkorting, MeetEenheid eenheid, String ond, Boolean metWind) {
		korteNaam = afkorting;
		onderdeel = ond;
		wind = metWind;
		meetEenheid = eenheid;
	}
	
	private OnderdeelAnu(String afkorting, MeetEenheid eenheid, String ond, Ronde ronde) {
		korteNaam = afkorting;
		onderdeel = ond;
		this.ronde = ronde;
		meetEenheid = eenheid;
	}
	
	private OnderdeelAnu(String afkorting, MeetEenheid eenheid, String ond, Ronde ronde, Boolean metWind) {
		korteNaam = afkorting;
		onderdeel = ond;
		this.ronde = ronde;
		wind = metWind;
		meetEenheid = eenheid;
	}

	public String getKorteNaam() {
		return korteNaam;
	}
	
	public static int getOnderdeelIndex (String onderdeel){
		OnderdeelAnu[] values = OnderdeelAnu.values();
		for (OnderdeelAnu o: values){
			if (o.getOnderdeel().equalsIgnoreCase(onderdeel)){
				return o.ordinal();
			}
		}
		return 999;
	}
	
	public static OnderdeelAnu getOnderdeelPositie (String onderdeel){
		OnderdeelAnu[] values = OnderdeelAnu.values();
		for (OnderdeelAnu o: values){
			if (o.getOnderdeel().equalsIgnoreCase(onderdeel)){
				return o;
			}
		}
		return null;
	}
	
	public static String getOnderdeel (String anuAfkorting){
		OnderdeelAnu[] values = OnderdeelAnu.values();
		for (OnderdeelAnu o: values){
			if (o.getKorteNaam().equalsIgnoreCase(anuAfkorting)){
				return o.getOnderdeel();
			}
		}
		return anuAfkorting;
	}
	
	public static String getOnderdeelKorteNaam (String anuAfkorting){
		for (OnderdeelAnu o: OnderdeelAnu.values()){
			if (o.getKorteNaam().equalsIgnoreCase(anuAfkorting)){
				return o.getKorteNaam();
			}
		}
		return anuAfkorting;
	}
	
	public static MeetEenheid getMeeteenheid (String anuAfkorting){
		for (OnderdeelAnu o: OnderdeelAnu.values()){
			if (o.getKorteNaam().equalsIgnoreCase(anuAfkorting)){
				return o.getMeetEenheid();
			}
		}
		return null;
	}
	
	public static Ronde getRonde (String anuAfkorting){
		for (OnderdeelAnu o: OnderdeelAnu.values()){
			if (o.getKorteNaam().equalsIgnoreCase(anuAfkorting)){
				return o.getRonde();
			}
		}
		return null;
	}
	
	public static Set<String> getOnderdelen(){
		for (OnderdeelAnu o: OnderdeelAnu.values()){
			onderdelen.add(o.getKorteNaam());
		}
		return onderdelen;
	}
	
	public static Set<String> getOnderdelenMetWind(){
		for (OnderdeelAnu o: OnderdeelAnu.values()){
			if(o.getWind()!= null && o.getWind()){
				onderdelenMetWind.add(o.getKorteNaam());	
			}
		}
		return onderdelenMetWind;
	}

	public String getOnderdeel() {
		return onderdeel;
	}

	public void setOnderdeel(String onderdeel) {
		this.onderdeel = onderdeel;
	}

	public Ronde getRonde() {
		return ronde;
	}

	public void setRonde(Ronde ronde) {
		this.ronde = ronde;
	}

	public Boolean getWind() {
		return wind;
	}

	public MeetEenheid getMeetEenheid() {
		return meetEenheid;
	}

}
