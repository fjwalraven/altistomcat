package nl.altis.atletiek.nu;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public enum Categorie {

	JJA("Jongens Junior A", "JA", "Man"),
	JJAKORT("JJA", "JA", "Man"),
	JJB("Jongens Junior B", "JB", "Man"),
	JJBKORT("JJB", "JB", "Man"),
	JJC("Jongens Junior C", "JC", "Man"),
	JJCKORT("JJC", "JC", "Man"),
	JJC1("Jongens Junior C1", "JC", "Man"),
	JJC2("Jongens Junior C2", "JC", "Man"),
	JJC1KORT("JJC1", "JC", "Man"),
	JJC2KORT("JJC2", "JC", "Man"),
	JJD("Jongens Junior D", "JD", "Man"),
	JJDKORT("JJD", "JD", "Man"),
	JJD1("Jongens Junior D1", "JD", "Man"),
	JJD2("Jongens Junior D2", "JD", "Man"),
	JJD1KORT("JJD1", "JD", "Man"),
	JJD2KORT("JJD2", "JD", "Man"),
	JPA("Jongens Pupil A", "JPA", "Man"),
	JPA1("Jongens Pupil A1", "JPA", "Man"),
	JPA2("Jongens Pupil A2", "JPA", "Man"),
	JPB("Jongens Pupil B", "JPB", "Man"),
	JPC("Jongens Pupil C", "JPC", "Man"),
	JPD("Jongens Pupil D", "JPD", "Man"),
	JMINI("Jongens Pupil Mini", "JPD", "Man"),
	JASP("Jongens Specials Junioren", "JSJ", "Man"),
	PASP("Jongens Specials Pupillen", "JSP", "Man"),
	MM("MM", "Mmas", "Man"),
	MMAS("Masters mannen", "Mmas", "Man"),
	MMA35("Masters mannen", "M35", "Man"),
	MMA35P("Masters Mannen 35+", "M35", "Man"),
	M35("Mannen 35", "M35", "Man"),
	MM35PLUS("Mannen Masters 35+", "M35", "Man"),
	MMA40("Masters mannen", "M40", "Man"),
	MMA40P("Masters mannen 40+", "M40", "Man"),
	M40("Mannen 40", "M40", "Man"),
	MM40PLUS("Mannen Masters 40+", "M40", "Man"),
	MMA45("Masters mannen", "M45", "Man"),
	MMA45P("Masters mannen 45+", "M45", "Man"),
	M45("Mannen 45", "M45", "Man"),
	MM45PLUS("Mannen Masters 45+", "M45", "Man"),
	MMA50("Masters mannen", "M50", "Man"),
	MMA50P("Masters mannen 50+", "M50", "Man"),
	M50("Mannen 50", "M50", "Man"),
	MM50PLUS("Mannen Masters 50+", "M50", "Man"),
	MMA55("Masters mannen", "M55", "Man"),
	MMA55P("Masters mannen 55+", "M55", "Man"),
	M55("Mannen 55", "M55", "Man"),
	MM55PLUS("Mannen Masters 55+", "M55", "Man"),
	MMA60("Masters mannen", "M60", "Man"),
	MMA60P("Masters mannen 60+", "M60", "Man"),
	M60("Mannen 60", "M60", "Man"),
	MM60PLUS("Mannen Masters 60+", "M60", "Man"),
	MMA65("Masters mannen", "M65", "Man"),
	MMA65P("Masters mannen 65+", "M65", "Man"),
	M65("Mannen 65", "M65", "Man"),
	MM65PLUS("Mannen Masters 65+", "M65", "Man"),
	MMA70("Masters mannen", "M70", "Man"),
	MMA70P("Masters mannen 70+", "M70", "Man"),
	M70("Mannen 70", "M70", "Man"),
	MM70PLUS("Mannen Masters 70+", "M70", "Man"),
	MMA75("Masters mannen", "M75", "Man"),
	MMA75P("Masters mannen 75+", "M75", "Man"),
	M75("Mannen 75", "M75", "Man"),
	MM75PLUS("Mannen Masters 75+", "M75", "Man"),
	MMA80("Masters mannen", "M80", "Man"),
	MMA80P("Masters mannen 80+", "M80", "Man"),
	M80("Mannen 80", "M80", "Man"),
	MM80PLUS("Mannen Masters 80+", "M80", "Man"),
	RMA35("Recreanten mannen", "M35", "Man"),
	RMA40("Recreanten mannen", "M40", "Man"),
	RMA45("Recreanten mannen", "M45", "Man"),
	RMA50("Recreanten mannen", "M50", "Man"),
	RMA55("Recreanten mannen", "M55", "Man"),
	RMA60("Recreanten mannen", "M60", "Man"),
	RMA65("Recreanten mannen", "M65", "Man"),
	RMA70("Recreanten mannen", "M70", "Man"),
	RMA75("Recreanten mannen", "M75", "Man"),
	RMA80("Recreanten mannen", "M80", "Man"),
	MSEN("Msen", "Senioren mannen", "Man"),
	MANNEN("Mannen", "Senioren mannen", "Man"),
	M("M", "Senioren mannen", "Man"),
	ALLEM("Alle Mannen", "Senioren mannen", "Man"),
	M_SENIOREN("Senioren mannen", "Senioren mannen", "Man"),
	WEDMAN("Wedstrijdatleten mannen", "Senioren mannen", "Man"),
	MJA("Meisjes Junior A", "MA", "Vrouw"),
	MJAKORT("MJA", "MA", "Vrouw"),
	MJB("Meisjes Junior B", "MB", "Vrouw"),
	MJBKORT("MJB", "MB", "Vrouw"),
	MJC("Meisjes Junior C", "MC", "Vrouw"),
	MJCKORT("MJC", "MC", "Vrouw"),
	MJC1("Meisjes Junior C1", "MC", "Vrouw"),
	MJC2("Meisjes Junior C2", "MC", "Vrouw"),
	MJC1KORT("MJC1", "MC", "Vrouw"),
	MJC2KORT("MJC2", "MC", "Vrouw"),
	MJD("Meisjes Junior D", "MD", "Vrouw"),
	MJDKORT("MJD", "MD", "Vrouw"),
	MJD1("Meisjes Junior D1", "MD", "Vrouw"),
	MJD2("Meisjes Junior D2", "MD", "Vrouw"),
	MJD1KORT("MJD1", "MD", "Vrouw"),
	MJD2KORT("MJD2", "MD", "Vrouw"),
	MPA("Meisjes Pupil A", "MPA", "Vrouw"),
	MPA1("Meisjes Pupil A1", "MPA", "Vrouw"),
	MPUPA1("MPA1", "MPA", "Vrouw"),
	MPA2("Meisjes Pupil A2", "MPA", "Vrouw"),
	MPUPA2("MPA2", "MPA", "Vrouw"),
	MPB("Meisjes Pupil B", "MPB", "Vrouw"),
	MPC("Meisjes Pupil C", "MPC", "Vrouw"),
	MPD("Meisjes Pupil D", "MPD", "Vrouw"),
	MMINI("Meisjes Pupil Mini", "MPD", "Vrouw"),
	MJASP("Meisjes Specials Junioren", "MSJ", "Vrouw"),
	MPASP("Meisjes Specials Pupillen", "MSP", "Vrouw"),
	VM("MV", "Vmas", "Vrouw"),
	VMAS("Masters Vrouwen", "Vmas", "Vrouw"),
	MAV35("Masters Vrouwen", "V35", "Vrouw"),
	MAV35P("Masters Vrouwen 35+", "V35", "Vrouw"),
	V35("Vrouwen 35", "V35", "Vrouw"),
	V35PLUS("Vrouwen Masters 35+", "V35", "Vrouw"),
	MAV40("Masters Vrouwen", "V40", "Vrouw"),
	MAV40P("Masters Vrouwen 40+", "V40", "Vrouw"),
	V40("Vrouwen 40", "V40", "Vrouw"),
	V40PLUS("Vrouwen Masters 40+", "V40", "Vrouw"),
	MAV45("Masters Vrouwen", "V45", "Vrouw"),
	MAV45P("Masters Vrouwen 45+", "V45", "Vrouw"),
	V45("Vrouwen 45", "V45", "Vrouw"),
	V45PLUS("Vrouwen Masters 45+", "V45", "Vrouw"),
	MAV50("Masters Vrouwen", "V50", "Vrouw"),
	MAV50P("Masters Vrouwen 50+", "V50", "Vrouw"),
	V50("Vrouwen 50", "V50", "Vrouw"),
	V50PLUS("Vrouwen Masters 50+", "V50", "Vrouw"),
	MAV55("Masters Vrouwen", "V55", "Vrouw"),
	MAV55P("Masters Vrouwen 55+", "V55", "Vrouw"),
	V55("Vrouwen 55", "V55", "Vrouw"),
	V55PLUS("Vrouwen Masters 55+", "V55", "Vrouw"),
	MAV60("Masters Vrouwen", "V60", "Vrouw"),
	MAV60P("Masters Vrouwen 60+", "V60", "Vrouw"),
	V60("Vrouwen 60", "V60", "Vrouw"),
	V60PLUS("Vrouwen Masters 60+", "V60", "Vrouw"),
	MAV65("Masters Vrouwen", "V65", "Vrouw"),
	MAV65P("Masters Vrouwen 65+", "V65", "Vrouw"),
	V65("Vrouwen 65", "V65", "Vrouw"),
	V65PLUS("Vrouwen Masters 65+", "V65", "Vrouw"),
	MAV70("Masters Vrouwen", "V70", "Vrouw"),
	MAV70P("Masters Vrouwen 70+", "V70", "Vrouw"),
	V70("Vrouwen 70", "V70", "Vrouw"),
	V70PLUS("Vrouwen Masters 70+", "V70", "Vrouw"),
	MAV75("Masters Vrouwen", "V75", "Vrouw"),
	MAV75P("Masters Vrouwen 75+", "V75", "Vrouw"),
	V75("Vrouwen 75", "V75", "Vrouw"),
	V75PLUS("Vrouwen Masters 75+", "V75", "Vrouw"),
	MAV80("Masters Vrouwen", "V80", "Vrouw"),
	MAV80P("Masters Vrouwen 80+", "V80", "Vrouw"),
	V80("Vrouwen 80", "V80", "Vrouw"),
	V80PLUS("Vrouwen Masters 80+", "V80", "Vrouw"),
	RAV35("Recreanten Vrouwen", "V35", "Vrouw"),
	RAV40("Recreanten Vrouwen", "V40", "Vrouw"),
	RAV45("Recreanten Vrouwen", "V45", "Vrouw"),
	RAV50("Recreanten Vrouwen", "V50", "Vrouw"),
	RAV55("Recreanten Vrouwen", "V55", "Vrouw"),
	RAV60("Recreanten Vrouwen", "V60", "Vrouw"),
	RAV65("Recreanten Vrouwen", "V65", "Vrouw"),
	RAV70("Recreanten Vrouwen", "V70", "Vrouw"),
	RAV75("Recreanten Vrouwen", "V75", "Vrouw"),
	RAV80("Recreanten Vrouwen", "V80", "Vrouw"),
	VROUWEN("Vrouwen", "Senioren vrouwen", "Vrouw"),
	VSEN("Vsen", "Senioren vrouwen", "Vrouw"),
	ALLEV("Alle Vrouwen", "Senioren vrouwen", "Vrouw"),
	V_SENIOREN("Senioren vrouwen", "Senioren vrouwen", "Vrouw"),
	V("V", "Senioren vrouwen", "Vrouw"),
	WEDVROUW("Wedstrijdatleten vrouwen", "Senioren vrouwen", "Vrouw");

	private Categorie(String langeNaam, String korteNaam, String manOfVrouw) {
		categorie = langeNaam;
		afkortingCategorie = korteNaam;
		geslacht = manOfVrouw;
	}

	private String categorie;
	private String afkortingCategorie;
	private String geslacht;
	private static List<String> categorien = new ArrayList<>();

	public String getCategorie() {
		return categorie;
	}

	public String getAfkortingCategorie() {
		return afkortingCategorie;
	}
	
	public String getGeslacht() {
		return geslacht;
	}

	public static List<String> getCategorieLijst(){
		for (Categorie c: Categorie.values()){
			categorien.add(c.getCategorie());
		}
		return categorien;
	}
	
	public static Set<String> getUniekeCategorieLijst(){
		Set<String> lijst = new HashSet<>();
		for (Categorie c: Categorie.values()){
			lijst.add(c.getAfkortingCategorie());
		}
		return lijst;
	}
	
	public static String getKorteNaam (String langeNaam){
		for (Categorie c: Categorie.values()){
			if (c.getCategorie().equalsIgnoreCase(langeNaam.trim())){
				return c.getAfkortingCategorie();
			}
		}
		return langeNaam;
	}
	
	public static String getKorteNaam (Categorie categorie){
		for (Categorie c: Categorie.values()){
			if (c == categorie){
				return c.getAfkortingCategorie();
			}
		}
		return null;
	}
	
	public static String getLangeNaam (Categorie categorie){
		for (Categorie c: Categorie.values()){
			if (c == categorie){
				return c.getCategorie();
			}
		}
		return null;
	}
	
	public static String getLangeNaam (String korteNaam){
		for (Categorie c: Categorie.values()){
			if (c.getAfkortingCategorie().equalsIgnoreCase(korteNaam)){
				return c.getCategorie();
			}
		}
		return null;
	}
	
	public static String getGeslacht (String langeNaam){
		for (Categorie c: Categorie.values()){
			if (c.getCategorie().equalsIgnoreCase(langeNaam.trim())){
				return c.getGeslacht();
			}
		}
		return langeNaam;
	}

}
