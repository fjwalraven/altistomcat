package nl.altis.atletiek.nu;

public enum Ronde {
	SERIES, ACHTSTE_FINALES, KWART_FINALES, HALVE_FINALES, FINALE;
}
