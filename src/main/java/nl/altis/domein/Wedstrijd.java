package nl.altis.domein;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="WEDSTRIJD")
public class Wedstrijd {
	
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private int wedstrijdId;
	
	private int anuId;
	
	private String datumVan;
	private String datumTot;
	private String indoorOfOutdoor;
	private String tijdWaarneming;
	private String naam;
	private String plaats;
	private int uitslagId;
	private String uitslagURL;
	
	@OneToMany (mappedBy="wedstrijd")
	private List<Resultaat> resultaten = new ArrayList<Resultaat>();
	
	@OneToMany (mappedBy="wedstrijd")
	private List<Onderdeel> onderdelen = new ArrayList<Onderdeel>();
	
	@ManyToMany (mappedBy="wedstrijden")
	private List<Atleet> atleten = new ArrayList<Atleet>();

	public int getWedstrijdId() {
		return wedstrijdId;
	}
	

	public void setWedstrijdId(int wedstrijdId) {
		this.wedstrijdId = wedstrijdId;
	}


	public String getTijdWaarneming() {
		return tijdWaarneming;
	}


	public void setTijdWaarneming(String tijdWaarneming) {
		this.tijdWaarneming = tijdWaarneming;
	}


	public String getDatumVan() {
		return datumVan;
	}


	public void setDatumVan(String datumVan) {
		this.datumVan = datumVan;
	}


	public String getDatumTot() {
		return datumTot;
	}


	public void setDatumTot(String datumTot) {
		this.datumTot = datumTot;
	}


	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public String getPlaats() {
		return plaats;
	}

	public void setPlaats(String plaats) {
		this.plaats = plaats;
	}

	public int getUitslagId() {
		return uitslagId;
	}

	public void setUitslagId(int uitslagId) {
		this.uitslagId = uitslagId;
	}

	public String getUitslagURL() {
		return uitslagURL;
	}

	public void setUitslagURL(String uitslagURL) {
		this.uitslagURL = uitslagURL;
	}

	public List<Resultaat> getResultaten() {
		return resultaten;
	}

	public void setResultaten(List<Resultaat> resultaten) {
		this.resultaten = resultaten;
	}

	public List<Onderdeel> getOnderdelen() {
		return onderdelen;
	}

	public void setOnderdelen(List<Onderdeel> onderdelen) {
		this.onderdelen = onderdelen;
	}

	public List<Atleet> getAtleten() {
		return atleten;
	}


	public void setAtleten(List<Atleet> atleten) {
		this.atleten = atleten;
	}


	public int getAnuId() {
		return anuId;
	}


	public void setAnuId(int anuId) {
		this.anuId = anuId;
	}


	public String getIndoorOfOutdoor() {
		return indoorOfOutdoor;
	}


	public void setIndoorOfOutdoor(String indoorOfOutdoor) {
		this.indoorOfOutdoor = indoorOfOutdoor;
	}

	public String displayWedstrijd(){
		StringBuilder sb = new StringBuilder();
		sb.append("----------------------");
		sb.append("\n");
		sb.append(datumVan);
		if (datumTot != null){
			sb.append(" - ");
			sb.append(datumTot);
		}
		sb.append("\n");
		sb.append(naam);
		sb.append(" - ");
		sb.append(plaats);
		sb.append("\n");
		sb.append(indoorOfOutdoor);
		sb.append(" - ");
		sb.append(tijdWaarneming);
		sb.append(" gemeten");
		sb.append("\n");
		sb.append(uitslagURL);
		sb.append("\n");
		sb.append("----------------------");
		return sb.toString();
	}

	@Override
	public String toString() {
		return "Wedstrijd [wedstrijdId=" + wedstrijdId + ", anuId=" + anuId + ", datumVan=" + datumVan + ", datumTot="
				+ datumTot + ", indoorOfOutdoor=" + indoorOfOutdoor + ", tijdWaarneming=" + tijdWaarneming + ", naam="
				+ naam + ", plaats=" + plaats + ", uitslagId=" + uitslagId + ", uitslagURL=" + uitslagURL;
//				+ ", resultaten=" + resultaten + ", onderdelen=" + onderdelen + ", atleten=" + atleten + "]";
	}
	
	

}
