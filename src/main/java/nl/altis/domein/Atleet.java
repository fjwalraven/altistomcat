package nl.altis.domein;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ATLEET")
public class Atleet {
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private int atleetId;
	private String naam;
	private int bondsnummer;
	@Temporal(TemporalType.DATE)
	private Date geboorteDatum;
	
	private String geslacht;
	private String vereniging;
	
	@ManyToMany
	@JoinTable(name = "ATLEET_WEDSTRIJD", 
	   joinColumns = {      	@JoinColumn(name = "ATLEET_ID", nullable = false) }, 
	   inverseJoinColumns = {	@JoinColumn(name = "WEDSTRIJD_ID", nullable = false) })
	private List<Wedstrijd> wedstrijden = new ArrayList<Wedstrijd>();;
	
	@OneToMany (mappedBy="atleet")
	private List<Resultaat> resultaten = new ArrayList<Resultaat>();

	

	public int getAtleetId() {
		return atleetId;
	}

	public void setAtleetId(int atleetId) {
		this.atleetId = atleetId;
	}

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public int getBondsnummer() {
		return bondsnummer;
	}

	public void setBondsnummer(int bondsnummer) {
		this.bondsnummer = bondsnummer;
	}

	

	public Date getGeboorteDatum() {
		return geboorteDatum;
	}

	public void setGeboorteDatum(Date geboorteDatum) {
		this.geboorteDatum = geboorteDatum;
	}

	

	public String getVereniging() {
		return vereniging;
	}

	public void setVereniging(String vereniging) {
		this.vereniging = vereniging;
	}

	public String getGeslacht() {
		return geslacht;
	}

	public void setGeslacht(String geslacht) {
		this.geslacht = geslacht;
	}

	public List<Wedstrijd> getWedstrijden() {
		return wedstrijden;
	}

	public void setWedstrijden(List<Wedstrijd> wedstrijden) {
		this.wedstrijden = wedstrijden;
	}

	public List<Resultaat> getResultaten() {
		return resultaten;
	}

	public void setResultaten(List<Resultaat> resultaten) {
		this.resultaten = resultaten;
	}

	@Override
	public String toString() {
		return "Atleet [atleetId=" + atleetId + ", naam=" + naam + ", bondsnummer=" + bondsnummer + ", geboorteDatum="
				+ geboorteDatum + ", geslacht=" + geslacht + ", vereniging=" + vereniging + "]";
	}
	
	
}
