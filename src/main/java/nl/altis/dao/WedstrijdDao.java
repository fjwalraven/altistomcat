package nl.altis.dao;

import java.text.ParseException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import nl.altis.domein.Wedstrijd;

public class WedstrijdDao {
	public Wedstrijd persistWedstrijd(Wedstrijd anuWedstrijd) throws ParseException {

		/* Create EntityManagerFactory */
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");

		/* Create EntityManager */
		EntityManager em = emf.createEntityManager();

		/* Persist entity */
		em.getTransaction().begin();

		em.persist(anuWedstrijd);

		em.getTransaction().commit();
		return anuWedstrijd;
	}

	public boolean isWedstrijdAanwezig(String atletieknuId) {
		Wedstrijd w = getWedstrijd(atletieknuId);
		if (w == null) {
			return false;
		}
		return true;
	}

	public Wedstrijd getWedstrijd(String atletieknuId) {
		/* Create EntityManagerFactory */
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		/* Create EntityManager */
		EntityManager em = emf.createEntityManager();

		String zoek = "SELECT w FROM Wedstrijd w WHERE w.anuId = " + new Integer(atletieknuId);
		TypedQuery<Wedstrijd> query = em.createQuery(zoek, Wedstrijd.class);
		List<Wedstrijd> resultList = query.getResultList();

		if (resultList.isEmpty()) {
			return null;
		}
		return resultList.get(0);
	}
	
	public Wedstrijd getWedstrijd(String naam, String datumVan, String datumTot) {
		/* Create EntityManagerFactory */
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		/* Create EntityManager */
		EntityManager em = emf.createEntityManager();
		String zoek;
		if (datumTot != null){
			zoek = "SELECT w FROM Wedstrijd w WHERE w.naam = '" + naam + "' AND w.datumVan = '"  + datumVan + "' AND w.datumTot = '" + datumTot + "'";
		} else {
			zoek = "SELECT w FROM Wedstrijd w WHERE w.naam = '" + naam + "' AND w.datumVan = '"  + datumVan + "'";
		}
		System.out.println(zoek);
		TypedQuery<Wedstrijd> query = em.createQuery(zoek, Wedstrijd.class);
		List<Wedstrijd> resultList = query.getResultList();

		if (resultList.isEmpty()) {
			return null;
		}
		return resultList.get(0);
	}
	
	public Wedstrijd getWedstrijdById(String wedstrijdId) {
		/* Create EntityManagerFactory */
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		/* Create EntityManager */
		EntityManager em = emf.createEntityManager();

		String zoek = "SELECT w FROM Wedstrijd w WHERE w.wedstrijdId = " + new Integer(wedstrijdId);
		TypedQuery<Wedstrijd> query = em.createQuery(zoek, Wedstrijd.class);
		List<Wedstrijd> resultList = query.getResultList();

		if (resultList.isEmpty()) {
			return null;
		}
		return resultList.get(0);
	}

	public Wedstrijd getWedstrijd(EntityManager em, String naam, String datumVan, String datumTot) {
		String zoek;
		if (datumTot != null){
			zoek = "SELECT w FROM Wedstrijd w WHERE w.naam = '" + naam + "' AND w.datumVan = '"  + datumVan + "' AND w.datumTot = '" + datumTot + "'";
		} else {
			zoek = "SELECT w FROM Wedstrijd w WHERE w.naam = '" + naam + "' AND w.datumVan = '"  + datumVan + "'";
		}
		System.out.println(zoek);
		TypedQuery<Wedstrijd> query = em.createQuery(zoek, Wedstrijd.class);
		List<Wedstrijd> resultList = query.getResultList();

		if (resultList.isEmpty()) {
			return null;
		}
		return resultList.get(0);
	}

}
