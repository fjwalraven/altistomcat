package nl.altis.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import nl.altis.domein.Categorie;
import nl.altis.domein.Onderdeel;

public class OnderdeelDao {
	
	public Onderdeel zoekOnderdeel(String korteNaam){
		/* Create EntityManagerFactory */
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");

		/* Create EntityManager */
		EntityManager em = emf.createEntityManager();
		
		String zoek = "SELECT o FROM Onderdeel o WHERE o.korteNaam = '" + korteNaam + "' ";
		System.out.println(zoek);
		TypedQuery<Onderdeel> query = em.createQuery(zoek, Onderdeel.class);
		List<Onderdeel> resultList = query.getResultList();
		
		return resultList.get(0);
	}
	
	public Onderdeel zoekOnderdeelWaarde(String korteNaam, Integer waarde){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		EntityManager em = emf.createEntityManager();
		
		String zoek = "SELECT o FROM Onderdeel o WHERE o.korteNaam = '" + korteNaam + "' AND o.waarde = " + waarde;
		System.out.println(zoek);	
		TypedQuery<Onderdeel> query = em.createQuery(zoek, Onderdeel.class);
		List<Onderdeel> resultList = query.getResultList();
		return resultList.get(0);
	}
	
	public Onderdeel zoekOnderdeelWaarde(String korteNaam, double waarde) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		EntityManager em = emf.createEntityManager();
		
		String zoek = "SELECT o FROM Onderdeel o WHERE o.korteNaam = '" + korteNaam + "' AND o.waarde = " + waarde;
		System.out.println(zoek);	
		TypedQuery<Onderdeel> query = em.createQuery(zoek, Onderdeel.class);
		List<Onderdeel> resultList = query.getResultList();
		return resultList.get(0);
	}
	
	public List<Onderdeel> zoekOnderdelen(String... korteNaam){
		List<Onderdeel> alles = new ArrayList<>();
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		EntityManager em = emf.createEntityManager();
		for (String o: korteNaam){
			String zoek = "SELECT o FROM Onderdeel o WHERE o.korteNaam = '" + o + "' ";
			System.out.println(zoek);
			TypedQuery<Onderdeel> query = em.createQuery(zoek, Onderdeel.class);
			List<Onderdeel> resultList = query.getResultList();
			alles.add(resultList.get(0));	
		}
		return alles;
	}
	
	public List<Onderdeel> zoekMeerkampOnderdelen(String categorie, String... korteNaam){
		List<Onderdeel> alles = new ArrayList<>();
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		EntityManager em = emf.createEntityManager();
		for (String o: korteNaam){
			String zoek = "SELECT o FROM Onderdeel o JOIN o.categorien c WHERE c.cat = '" + categorie + "' AND LOWER(o.korteNaam) = '" + o.toLowerCase() + "' ";
			System.out.println(zoek);
			TypedQuery<Onderdeel> query = em.createQuery(zoek, Onderdeel.class);
			List<Onderdeel> resultList = query.getResultList();
			alles.add(resultList.get(0));	
		}
		return alles;
	}
	
	public List<Integer> zoekMeerkampOnderdelenIds(String categorie, String... korteNaam){
		List<Integer> alles = new ArrayList<>();
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		EntityManager em = emf.createEntityManager();
		for (String o: korteNaam){
			String zoek = "SELECT o FROM Onderdeel o JOIN o.categorien c WHERE c.cat = '" + categorie + "' AND LOWER(o.korteNaam) = '" + o.toLowerCase() + "' ";
			System.out.println(zoek);
			TypedQuery<Onderdeel> query = em.createQuery(zoek, Onderdeel.class);
			List<Onderdeel> resultList = query.getResultList();
			alles.add(resultList.get(0).getOnderdeelId());	
		}
		return alles;
	}

	public Onderdeel zoekOnderdeelExtraWaarde(String naam, double extra) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		EntityManager em = emf.createEntityManager();
		
		String zoek = "SELECT o FROM Onderdeel o WHERE o.korteNaam = '" + naam + "' AND o.extraWaarde = " + extra;
		System.out.println(zoek);		
		TypedQuery<Onderdeel> query = em.createQuery(zoek, Onderdeel.class);
		List<Onderdeel> resultList = query.getResultList();
		return resultList.get(0);
	}

	public Onderdeel zoekOnderdeel(String ond, String categorie) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		EntityManager em = emf.createEntityManager();
		
		String zoek = "SELECT o FROM Onderdeel o JOIN o.categorien c WHERE c.cat = '" + categorie + "' AND o.korteNaam = '" + ond + "'";
		System.out.println(zoek);	
		TypedQuery<Onderdeel> query = em.createQuery(zoek, Onderdeel.class);
		List<Onderdeel> resultList = query.getResultList();
		return resultList.get(0);
	}

	public Onderdeel zoekMeerkamp(String categorie, List<String> onderdelenMeerkamp) {
		
		List<String> omk = new ArrayList<>(onderdelenMeerkamp);
		
		System.out.println(omk);
		omk.remove("Punten totaal");
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		EntityManager em = emf.createEntityManager();
		
		String zoek = "SELECT o FROM Onderdeel o JOIN o.categorien c WHERE c.cat = '" + categorie + "' AND o.meerkampOnderdelen IS NOT EMPTY";
		System.out.println(zoek);		
		TypedQuery<Onderdeel> query = em.createQuery(zoek, Onderdeel.class);
		List<Onderdeel> resultList = query.getResultList();
		System.out.println("Aantal gevonden meerkampen: " + resultList.size());
		
		for (int i= 0; i<resultList.size();i++){
			Onderdeel mk = resultList.get(i);
			List<Integer> mkOnderdelen = mk.getMeerkampOnderdelenIds();
			if (omk.size() == mkOnderdelen.size()){
				String[] onderdelenArray = new String[omk.size()];
				onderdelenArray = omk.toArray(onderdelenArray);
				List<Integer> meerkampOnderdelen = zoekMeerkampOnderdelenIds(categorie, onderdelenArray);
				if (meerkampOnderdelen.containsAll(mkOnderdelen)){
					System.out.println("dezelfde meerkamp gevonden");
					return mk;
				}
			}
		}
		return null;
	}

	public Onderdeel maakMeerkamp(String categorie, List<String> onderdelenMeerkamp) {
		List<String> individueleOnderdelen = new ArrayList<>(onderdelenMeerkamp);
		individueleOnderdelen.remove("Punten totaal");
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		CategorieDao catDao = new CategorieDao();
		nl.altis.domein.Categorie cat = catDao.zoekCategorie(categorie);
		
		// Zoek de onderdelen in de database voor de categorie van de meerkamp
		String[] onderdelen = new String[individueleOnderdelen.size()];
		individueleOnderdelen.toArray(onderdelen);
		List<Onderdeel> mkOnderdelen = zoekMeerkampOnderdelen(categorie, onderdelen);
		
		Onderdeel meerkamp = new Onderdeel();
		meerkamp.setKorteNaam("meerkamp");
		meerkamp.setLangeNaam("" + mkOnderdelen.size() + "-kamp " + categorie + " " + Arrays.stream(onderdelen).collect(Collectors.joining(",")));
		meerkamp.getCategorien().add(cat);
		meerkamp.setRegistratieDatum(new Date());
		meerkamp.setUser("fwn001");
		em.persist(meerkamp);
		// Zet de categorie
		cat.getOnderdelen().add(meerkamp);
		em.merge(cat);
		// koppel de onderdelen aan de meerkamp
		for (Onderdeel o: mkOnderdelen){
			o.getMeerkamp().add(meerkamp);
			em.merge(o);
			meerkamp.getMeerkampOnderdelen().add(o);
		}
		em.merge(meerkamp);
		em.getTransaction().commit();
		return meerkamp;
	}

	public Onderdeel zoekOnderdeelExtraWaardeToel(String naam, double extra, String toelichting) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");
		EntityManager em = emf.createEntityManager();
		
		String zoek = "SELECT o FROM Onderdeel o WHERE o.korteNaam = '" + naam + "' AND o.extraWaarde = " + extra + " AND o.toelichting = " + toelichting;
		System.out.println(zoek);		
		TypedQuery<Onderdeel> query = em.createQuery(zoek, Onderdeel.class);
		List<Onderdeel> resultList = query.getResultList();
		return resultList.get(0);
	}

	public Onderdeel zoekOnderdeel(String onderdeel, Categorie categorie) {
		return zoekOnderdeel(onderdeel, categorie.getCat());
	}

	public Onderdeel zoekOnderdeel(EntityManager em, String ond, Categorie categorie) {
		String zoek = "SELECT o FROM Onderdeel o JOIN o.categorien c WHERE c.cat = '" + categorie.getCat() + "' AND o.korteNaam = '" + ond + "'";
		System.out.println(zoek);	
		TypedQuery<Onderdeel> query = em.createQuery(zoek, Onderdeel.class);
		List<Onderdeel> resultList = query.getResultList();
		return resultList.isEmpty() ? null : resultList.get(0);
	}

	public Onderdeel zoekOnderdeelLangeNaam(EntityManager em, String ond, Categorie categorie) {
		String zoek = "SELECT o FROM Onderdeel o JOIN o.categorien c WHERE c.cat = '" + categorie.getCat() + "' AND o.langeNaam = '" + ond + "'";
		System.out.println(zoek);	
		TypedQuery<Onderdeel> query = em.createQuery(zoek, Onderdeel.class);
		List<Onderdeel> resultList = query.getResultList();
		return resultList.isEmpty() ? null : resultList.get(0);
	}

	public Onderdeel zoekOnderdeelToelichting(EntityManager em, String onderdeel, Categorie categorie) {
		String zoek = "SELECT o FROM Onderdeel o JOIN o.categorien c WHERE c.cat = '" + categorie.getCat() + "' AND o.toelichting = '" + onderdeel + "'";
		System.out.println(zoek);	
		TypedQuery<Onderdeel> query = em.createQuery(zoek, Onderdeel.class);
		List<Onderdeel> resultList = query.getResultList();
		return resultList.isEmpty() ? null : resultList.get(0);
	}

}
