package nl.altis.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import nl.altis.domein.Categorie;

public class CategorieDao {

	public Categorie zoekCategorie(String naam) {
		/* Create EntityManagerFactory */
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("AltisBesten");

		/* Create EntityManager */
		EntityManager em = emf.createEntityManager();

		// escape single quote with two single quotes
		if (naam.contains("'")) {
			System.out.println(naam);
			naam = naam.replace("'", "''");
		}

		String zoek = "SELECT c FROM Categorie c WHERE c.cat = '" + naam + "' ";

		TypedQuery<Categorie> query = em.createQuery(zoek, Categorie.class);
		List<Categorie> resultList = query.getResultList();

		if (resultList.isEmpty()) {
			return null;
		}

		return resultList.get(0);
	}

	public Categorie zoekCategorie(EntityManager em, String naam) {
		// escape single quote with two single quotes
		if (naam.contains("'")) {
			System.out.println(naam);
			naam = naam.replace("'", "''");
		}

		String zoek = "SELECT c FROM Categorie c WHERE c.cat = '" + naam + "' ";

		TypedQuery<Categorie> query = em.createQuery(zoek, Categorie.class);
		List<Categorie> resultList = query.getResultList();

		if (resultList.isEmpty()) {
			return null;
		}

		return resultList.get(0);
	}

}
