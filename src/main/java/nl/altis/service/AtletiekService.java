package nl.altis.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import nl.altis.dao.AtleetDao;
import nl.altis.dao.ResultaatDao;
import nl.altis.domein.Atleet;
import nl.altis.domein.Resultaat;

/**
 * Bean implementation class AtletiekService
 */
public class AtletiekService {
	
	
	private AtleetDao atleetDao = new AtleetDao();
	
	
	private ResultaatDao resultaatDao = new ResultaatDao();

	
	public Atleet readAtleet(String id, EntityManager em){
		return atleetDao.zoekAtleetById(id, em);
	}
	
	public List<Resultaat> getResultatenAtleet(String naam, EntityManager em){
		return resultaatDao.getDeelnemerResultaten(naam, em);
	}
	
	public List<Resultaat> getResultatenPeriode(EntityManager em){
		String datumVan = "2018-03-31";
		String datumTot = "2018-10-10";
		
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date datumVanaf;
		Date datumTotEnMet;
		try {
			datumVanaf = formatter.parse(datumVan);
			datumTotEnMet = formatter.parse(datumTot);
			List<Resultaat> deelnemerResultaten = resultaatDao.getResultatenByPeriode(em, datumVanaf, datumTotEnMet);
			return deelnemerResultaten;	
		} catch (ParseException e) {
			throw new IllegalArgumentException(e);
		}
		
	}

    /**
     * Default constructor. 
     */
    public AtletiekService() {
        // TODO Auto-generated constructor stub
    }

	public List<String> getNamen(EntityManager em) {
		return atleetDao.getAlleNamenAtleten(em);
	}

}
