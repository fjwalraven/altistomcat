package nl.altis.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import nl.altis.atletiek.nu.OnderdeelAnu;
import nl.altis.domein.Onderdeel;
import nl.altis.domein.Resultaat;
import nl.altis.service.AtletiekService;
import nl.altis.servlet.listener.EntityManagerFactoryServletContextListener;
import nl.altis.util.CategorieUtil;

/**
 * Servlet implementation class AtletiekServlet
 */
public class AtletiekServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AtletiekServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("doGet");
		
		EntityManager em = EntityManagerFactoryServletContextListener.createEntityManager();
		AtletiekService service = new AtletiekService();
		
		List<String> atleten = service.getNamen(em);
		String json = new Gson().toJson(atleten);
		request.setAttribute("atleten", json);
		
		String categorie = request.getParameter("cat");
		if (categorie!=null && !categorie.isEmpty()) {
			maakCategorieTabel(service, request, response, categorie, em);
		}
		
		String naam = request.getParameter("naam");
		if (naam!=null && !naam.isEmpty()) {
			maakNaamTabel(service, request, response, naam, em);

		}
		
		if (naam == null && categorie == null) {
			maakPeriodeTabel(service, request, response, em);	
		}
		em.close();
	}

	private void maakNaamTabel(AtletiekService service, HttpServletRequest request, HttpServletResponse response, String naam, EntityManager em)
			throws ServletException, IOException {
		List<Resultaat> resultatenAtleet = service.getResultatenAtleet(naam, em);
		// lijst per Onderdeel
		Map<Onderdeel, List<Resultaat>> catOnd = resultatenAtleet.stream().collect(Collectors.groupingBy(Resultaat::getOnderdeel));	
		// sorteer de resultaten
		sorteerResultaten(catOnd);
		Set<Onderdeel> onderdelen = catOnd.keySet();
		List<Onderdeel> onderdeelNamen = new ArrayList<>(onderdelen);

		// sorteer op volgorde van de OnderdeelAnu lijst
		Comparator<Onderdeel> comp = (Onderdeel a, Onderdeel b) -> {
			if (OnderdeelAnu.getOnderdeelIndex(a.getKorteNaam()) == 999 && OnderdeelAnu.getOnderdeelIndex(b.getKorteNaam()) == 999) {
				return b.getLangeNaam().compareTo(a.getLangeNaam());
			}
			return Integer.valueOf(OnderdeelAnu.getOnderdeelIndex(a.getKorteNaam()))
					.compareTo(Integer.valueOf(OnderdeelAnu.getOnderdeelIndex(b.getKorteNaam())));
		};
		Collections.sort(onderdeelNamen, comp);

//		for (Onderdeel ond : onderdeelNamen) {
//			System.out.println(OnderdeelAnu.getOnderdeelIndex(ond.getKorteNaam()) + " " + ond.getKorteNaam());
//		}
		
		request.setAttribute("keySet", onderdeelNamen);
		request.setAttribute("resMap", catOnd);
		request.setAttribute("naam", naam);
		request.getRequestDispatcher("/WEB-INF/resultatenPerNaam.jsp").forward(request, response);
	}

	private void maakCategorieTabel(AtletiekService service, HttpServletRequest request, HttpServletResponse response, String categorie, EntityManager em)
			throws ServletException, IOException {
		List<Resultaat> deelnemerResultaten = service.getResultatenPeriode(em);
		
		// lijst per Categorie, per Onderdeel
		Map<String, Map<Onderdeel, List<Resultaat>>> catOnd = deelnemerResultaten.stream().collect(
				Collectors.groupingBy(Resultaat::getCategorie, Collectors.groupingBy(Resultaat::getOnderdeel)));
		// resultaten per meegegeven Categorie
		Map<Onderdeel, List<Resultaat>> resMap = catOnd.get(categorie);
		// sorteer de resultaten
		sorteerResultaten(resMap);
		
		
		List<Onderdeel> onderdeelNamen = new ArrayList<>(resMap.keySet());
		// Sorteer de Onderdelen op volgorde van de OnderdeelAnu lijst
		Comparator<Onderdeel> comp = (Onderdeel a, Onderdeel b) -> {
			return Integer.valueOf(OnderdeelAnu.getOnderdeelIndex(a.getKorteNaam()))
					.compareTo(Integer.valueOf(OnderdeelAnu.getOnderdeelIndex(b.getKorteNaam())));
		};
		Collections.sort(onderdeelNamen, comp);

		request.setAttribute("keySet", onderdeelNamen);
		request.setAttribute("resMap", resMap);

		request.getRequestDispatcher("/WEB-INF/" + categorie.toLowerCase() + ".jsp").forward(request, response);
	}

	private void sorteerResultaten(Map<Onderdeel, List<Resultaat>> resMap) {
		for (List<Resultaat> resOnd: resMap.values()) {
			if (resOnd.get(0).getTijd() != null) {
				// sorteer op tijd
				Comparator<Resultaat> bijTijd = Comparator.comparing(r -> r.getTijd());
				resOnd.sort(bijTijd);
			} else if (resOnd.get(0).getAfstand() != null) {
				// sorteer op afstand
				Comparator<Resultaat> bijAfstand = Comparator.comparing(r -> r.getAfstand());
				resOnd.sort(bijAfstand.reversed());
			} else {
				// sorteer op punten
				Comparator<Resultaat> bijPunten = Comparator.comparing(r -> r.getPunten());
				resOnd.sort(bijPunten.reversed());
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("doPost");
		EntityManager em = EntityManagerFactoryServletContextListener.createEntityManager();
		AtletiekService service = new AtletiekService();
				
		String naam = request.getParameter("naam");

		maakNaamTabel(service, request, response, naam, em);
		em.close();
	}

	private void maakPeriodeTabel(AtletiekService service, HttpServletRequest request, HttpServletResponse response, EntityManager em)
			throws ServletException, IOException {
		List<Resultaat> deelnemerResultaten = service.getResultatenPeriode(em);
		// Verdeel de resultaten onder in categorie
		Map<String, List<Resultaat>> catOnd = deelnemerResultaten.stream().collect(Collectors.groupingBy(Resultaat::getCategorie));
		
		// lijst met alle categorien
		List<String> cats = new ArrayList<>(catOnd.keySet());

		List<String> pupillenUitslagen = cats.stream().filter(c -> CategorieUtil.getPupillen().contains(c)).collect(Collectors.toList());
		pupillenUitslagen.sort(Comparator.comparing(CategorieUtil.getPupillen()::indexOf));
		request.setAttribute("pup", pupillenUitslagen);

		List<String> juniorenUitslagen = cats.stream().filter(c -> CategorieUtil.getJunioren().contains(c)).collect(Collectors.toList());
		juniorenUitslagen.sort(Comparator.comparing(CategorieUtil.getJunioren()::indexOf));
		request.setAttribute("jun", juniorenUitslagen);

		List<String> seniorenUitslagen = cats.stream().filter(c -> CategorieUtil.getSenioren().contains(c)).collect(Collectors.toList());
		seniorenUitslagen.sort(Comparator.comparing(CategorieUtil.getSenioren()::indexOf));
		request.setAttribute("sen", seniorenUitslagen);

		List<String> mastersUitslagen = cats.stream().filter(c -> CategorieUtil.getMasters().contains(c)).collect(Collectors.toList());
		mastersUitslagen.sort(Comparator.comparing(CategorieUtil.getMasters()::indexOf));
		request.setAttribute("mas", mastersUitslagen);

		int max = CategorieUtil.max(pupillenUitslagen.size(), juniorenUitslagen.size(), seniorenUitslagen.size(),
				mastersUitslagen.size());
		request.setAttribute("max", max);

		request.getRequestDispatcher("/WEB-INF/result.jsp").forward(request, response);
	}

}
